rlab/ansible-tinc
=================

This ansible playbook downloads, compiles and installs tinc (1.1-pre16 by default).
And then it generates host and configuration files, and starts the tinc instance.

## Features

 - Out-of-box working and scalable tinc deployment
 - [tinc-vpn](http://tinc-vpn.org/) itself provides the following
   - Encryption
   - Mesh Networking
 - No SPOF
 - Layer 2 only for now

## Use cases

 - VPN ?
 - Encrypted overlay networking for hypervisors
 - Overlay network across multiple data centers

## Requirements

 - SSH access to all hosts
 - Connect to hosts at least once using ssh manually, to add them to known hosts
   - Or, I wouldn't do but, you can also disable host key checking in the [ansible.cfg](ansible.cfg)
 - Python installed on all hosts
 - Hosts needs to be either Debian or ArchLinux based
 - Obviously you need ansible

## Steps

Create `config/myvpn1` folder, give it any name you like. You can also copy [`config/example`](config/example) folder if you like.

And run;

```bash
ansible-playbook -i example/myvpn1/hosts.ini apply.yaml`
```

That's all. You have your encrypted L2 Mesh network.

**It will start the network in the final step.**

## Adding another host

Just edit configuration and run the playbook again.

Minimum configuration necessary for each host is,

 - add host to `hosts.ini`
 - set ip of that host in `group_vars/all.yml`

## TODO

 * [ ] Contributing documentation
 * [ ] Support for other mainstream distributions / package managers
 * [ ] Cleanup no longer existing hosts from hosts directory
 * [ ] L3 support (Mode=router)
 * [ ] Playbook for removing a host
 * [ ] Scripts or playbook for adding a host offline (no ssh access)
   - This is primarily necessary for adding mobile machines, developer boxes,
     etc. You don't get to ssh into customer's laptop.
 * [ ] Run `make` as non-root ?
 * [ ] Support configuration for static routes
 * [ ] Optinally support for running [tinc inside a container](https://git.rlab.io/system/tinc),
       instead of compiling it.
 * [ ] Support for (proper) init systems / process managers (OpenRC, sysvinit,
       supervisord, daemontools)
 * [ ] Backend option for [vpncloud.rs](https://github.com/dswd/vpncloud.rs)
       instead of tinc (would require a different project name than
       `tinc-ansible`)

## Contributing

MRs are very welcome. Please clone/fork it in anywhere you like, when you're done,
send an email to merge-requests@rlab.io with;
 - Title as; `[system/tinc-ansible] Merge: {feature explanation here, without brackets}`
 - Description, reasoning, etc.
 - Access information (eg. URL) to your repository/branch.
   - Or you can add patch files if you like, instead of a URL.

Please don't worry about perfection or being rejected, etc.

As a secondary option, contributions will be accepted from the [official mirror](https://gitlab.com/rainlab-inc/system/tinc-ansible) at gitlab.com.

## LICENSE

GPL-3.0, Please see [LICENSE](LICENSE)

Copyright 2018, Rainlab Tokyo

https://rainlab.co.jp
